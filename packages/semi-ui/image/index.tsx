import Image from "./image";
import PreviewInner from "./previewInner";
import Preview from "./preview";

export default Image;
export {
    PreviewInner,
    Preview,
}; 

export {
    ImageProps,
    PreviewImageProps,
    PreviewProps,
} from "./interface";